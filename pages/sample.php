<?php
/* *
 * Ridestats.pl Main Page
 * * pages/home.php
 *
 * @package		RideStats
 * @author		Dawid Dziurdzia
 * @copyright	© 2011
 *
 * */

class Home extends Controller{
	
	public function index($params)
	{	
		//newsy
		$news = DbnewsQuery::create()->limit(3)->orderByDate('DESC')->find();
		$this->display->news = $news;
	}

	public function contact($params)
	{
		
	}
	
	public function regulamin($params)
	{
		
	}
	
	public function privacy($params)
	{
		
	}
}

?>