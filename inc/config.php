<?php

/* *
 * Basic configuration
 * * inc/config.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	© 2011
 *
 * */
 
//DIRECTORIES
define('SESSION_NAME',    'framework');
define('INC_DIR', 		  ROOT_DIR.'/inc/');
define('LIB_DIR', 		  ROOT_DIR.'/libs/');
define('TEMPLATES_DIR',   ROOT_DIR.'/templates/');
define('CONTROLLERS_DIR', ROOT_DIR.'/pages/');
define('UPLOAD_DIR', 	  ROOT_DIR.'/uploads/');
define('STATIC_DIR', 	  ROOT_DIR.'/webroot/');

require ROOT_DIR.'inc/config.path.php';

//DEBUG
define('__DEBUG', 		  false); //create logs and show detailed messages
define('__AUTOLOADDEBUG', false);
define('__DEBUG_DISPLAY', __DEBUG&&false); //show messages on screen?
define('__DEBUG_PATH',    ROOT_DIR.'logs/framework.log'); //log filename
define('__DEBUG_PATH_DB', ROOT_DIR.'logs/db.log'); //log filename

//EXTENSIONS
define('TEMPLATES_EXT', '.php');

//SECRETS
define('SESSION_SECRET', 	'fghdfgjhdfgjfgkrt6u5bjueun44uwrtrbeueu653bu674ibi6j456j45w456j456j45jw4j2j45j554j');
define('PASSWORD_SECRET',	'dfhwrtutrjt4uwrtrbeueu653bu674ibi6j456j45eun4uwrbjueun4uwrtrbeu653bu674ibii4i7b7u');
define('ID_SECRET', 		'fbjueun4uwrtrbeueu653bbjueun4uwrtrbeu653bu674ibi674ibitrbeueuthyrty6j45j2j45j554j');
define('CRON_SECRET', 		'fghdfgjhdfgjfgkrt6ubjueun4uwrtrbeueu653bu67jfgjjfgj44gd4ibi6j456j45jw45j2j45j554j');
?>
