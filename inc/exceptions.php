<?php

/* *
 * Exception types
 * * inc/exceptions.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	© 2011
 *
 * */
//MVC exceoptions
class ViewException extends Exception {}
class RouteException extends Exception {}
class ControllerException extends Exception {}
?>