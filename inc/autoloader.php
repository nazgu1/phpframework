<?php
/* *
 * Autoloader class
 * * inc/autoloader.php
 *
 * @package             NazgulFramework
 * @author              Dawid Dziurdzia
 * @copyright   © 2011
 *
 * */

class ClassAutoloader {
	public function __construct() {
		spl_autoload_register(array($this, 'loader'));
	}
	private function loader($className) {
		$className = array_pop(explode('\\', $className));
		if(__DEBUG && __AUTOLOADDEBUG)
			echo('Loading class: '.$className);
		if($className==strtoupper($className))
			$path = ROOT_DIR.'class/templates/'.strtolower($className).'.php';
		else
			$path = ROOT_DIR.'class/'.strtolower($className).'.php';
			
		if(file_exists($path)){
			require($path);
			return true;
		}
		return false;
	}
}

$autoloader = new ClassAutoloader();