<?php
/* *
 * File with necessary functions
 * * bootstrap.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */
 
define('YES', true);
define( 'NO', false);

/**
 * Get either a Gravatar URL or complete image tag for a specified email address.
 *
 * @param string $email The email address
 * @param string $s Size in pixels, defaults to 80px [ 1 - 512 ]
 * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
 * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
 * @param boole $img True to return a complete IMG tag False for just the URL
 * @param array $atts Optional, additional key/value attributes to include in the IMG tag
 * @return String containing either just a URL or a complete image tag
 * @source http://gravatar.com/site/implement/images/php/
 */
function get_gravatar( $email, $s = 80, $d = 'identicon', $r = 'g', $img = false, $atts = array() ) {
	$url = 'http://www.gravatar.com/avatar/';
	$url .= md5(strtolower(trim($email)));
	$url .= "?s=$s&d=$d&r=$r";
	if ( $img ) {
		$url = '<img src="' . $url . '"';
		foreach ( $atts as $key => $val )
			$url .= ' ' . $key . '="' . $val . '"';
		$url .= ' />';
	}
	return $url;
}

function url($url, $echo=true){
	if(!$echo)
		return FRAMEWORK_URI.'/'.$url;
	echo FRAMEWORK_URI.'/'.$url;
}

function d($value,$default=NULL)
{
	return (isset($value)) ? $value : $default;
}

function da($array, $key, $default=NULL) {
	if(array_key_exists($key, $array))
		return d($array[$key], $default);
	return $default;
}

function dc($array, $key, $config_key) {
	return d($array[$key], c($config_key));
}

//TODO: make it working ;)
function c($key=NULL, $d=NULL)
{
	if(isset($key)) {
		$ret = Settings::instance()->get($key);
		return d($ret, $d);
	}
			
	return null;
}

function l($k,$d=NULL)
{
	return Localization::instance()->get($k,$d);
}

function lc($k,$c,$d=NULL)
{
	return Localization::instance()->getc($k,$c,$d);
}

function plural($c) {
	return Localization::instance()->plural_type($c);
}

?>