<?php
/* *
 * Strategy (view) class
 * * class/displaycontext.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	© 2011
 *
 * */

//klasa odpowiadająca za przydzielenie odpowiedniego widoku w zależności od strategii
class DisplayContext
{
	private $strategy = NULL;
	private $errors = array();
	
	//konstruktor
    public function __construct($p, $s='html') {
    	//w zależności od strategii przypisujemy widok
        switch ($s) {
            case "html": 
                $this->strategy = new View($p);
            break;
            case "json": 
                $this->strategy = new JSONView($p);
            break;
            case "soap": 
                $this->strategy = new SOAPView($p);
            break;
            default:
            	$this->strategy = new View($p);
            break;
        }
    }
    
    //magiczne metody do przypisywania do widoku zmiennych szablonu
    public function __set($name, $value) {
   		return $this->strategy->__set($name, $value);
    }
    
    public function __get($name) {
   		return $this->strategy->__get($name);
    }
    
    public function __isset($name) {
   		return $this->strategy->__isset($name);
    }
    
    public function __unset($name) {
   		return $this->strategy->__unset($name);
    }
    
    //renderowanie szablonu
    public function __toString() {
    	 return $this->strategy->__toString();
    }
}

?>
