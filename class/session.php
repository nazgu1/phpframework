<?php
/* *
 * Session handler
 * * inc/session.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */
 
 //TODO: session info in db & optionaly memcache
namespace NF;

class Session {

	private function __construct(){
		
	}
	
	private static function generateSessionSalt() {
		return md5($_SERVER['HTTP_USER_AGENT'].$_SERVER['REMOTE_ADDR'].SESSION_SECRET);
	}
	
	public static function redir($url) {
		if(!isset($_SESSION))
			Session::start();
		$_SESSION['redir'] = $url;
	}
	
	public static function flash($message, $type){
		if(!isset($_SESSION))
			Session::start();
		$_SESSION['flash'] = array($message, $type);
	}
	
	public static function show_flash()
	{
		if(!isset($_SESSION))
			Session::start();
		if(array_key_exists('flash', $_SESSION)){
			$s =  $_SESSION['flash'];
			unset($_SESSION['flash']);
			
			echo '<div class="row" id="flash">
<div class="six columns offset-by-three"><div class="alert-box '.$s[1].'">'.$s[0].
				'<a href="" class="close">&times;</a></div></div></div>';
		}
	}

	public static function start()
	{	
		ini_set('session.hash_function', 'whirlpool'); //ustawienie algorytmu hashowania sesji (generacja sid)
		session_set_cookie_params(2592000); //30 dni waznosc sesji
		session_start();
		
		if(function_exists('session_regenerate_id'))
			session_regenerate_id();
		
		if (isset($_SESSION['SESSION_SECURE_SALT'])) {
    		if ($_SESSION['SESSION_SECURE_SALT'] != Session::generateSessionSalt())
    		{
    			unset($_SESSION);
        		$_SESSION = array();
        		session_destroy();
        		return FALSE;
    		}
    	}
		else{
    		$_SESSION['SESSION_SECURE_SALT'] = Session::generateSessionSalt();
    	}
		
		return TRUE;
	}

	public static function save()
	{
		session_write_close();
	}
	
	public static function set($key, $value)
	{
		if(!isset($_SESSION))
			Session::start();
		$tmp = $_SESSION[$key];
		$_SESSION[$key] = $value;
		return $tmp;
	}
	
	public static function get($key) {
		if(!isset($_SESSION))
			Session::start();
		if(!array_key_exists($key, $_SESSION))
			return FALSE;
		return $_SESSION[$key];
	}
	
	public static function destroy()
	{
		unset($_SESSION);
		session_destroy();
	}
}

?>