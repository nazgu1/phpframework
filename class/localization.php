<?php
/* *
 * Localization class
 * * inc/localization.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */
 
class Localization
{
	protected $l = array();
	
	protected static $_instance;
	
	private function __construct(){
		$this->load();
	}
	
	public function instance()	{
		if(!isset(self::$_instance))
			self::$_instance = new Localization();
		return self::$_instance;
	}
	
	public function load($lang='pl')
	{
		$this->l = require(ROOT_DIR.'lang/'.$lang.'.php');
	}

	/**
	 * Zwrócenie zlokalizwanego ciągu
	 *
	 * @param String $k klucz stringa
	 * @param String $d domyślna wartość
	 * @return String
	 */
	function get($k,$d=NULL)
	{	
		if(isset($this->l))
			if(array_key_exists($k,$this->l))
				return $this->l[$k];
		
		if(isset($d))
			return $d;
			
		return $k;
	}
	
	/**
	 * Zwrócenie zlokalizwanego ciągu odmienionego dla odpwiedniej ilości
	 * (np 1 gałąź, 2 gałężie, 5 gałęzi)
	 *
	 * @param String $k klucz stringa
	 * @param Integer $c liczebność
	 * @param String $d domyślna wartość
	 * @return String
	 */
	function getc($k,$c,$d=NULL)
	{	
		$i = plural($c);
		if(isset($this->l))
			if(array_key_exists($k,$this->l))
				return $this->l[$k][$i];
		
		if(isset($d))
			return $d;
			
		return $k;
	}
	
	/**
	 * Zwrócenie typu liczby mnogiej w zależności od języka i liczebności
	 *
	 * @param Integer $c liczebność
	 * @return Integer (0-pojedyncza, 1-mnoga, 2-mnoga alternatywna)
	 */
	function plural_type($c) {
		$pl = eval(l('is_plural','if($c<=1){ return 0;} else {return 1;}'));
		if(!$pl)
			return 0;
			
		return $pl;
	}
}

?>