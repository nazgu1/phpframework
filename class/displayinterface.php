<?php
/* *
 * Template (view) class
 * * class/displayinterface.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	© 2011
 *
 * */

//interfejs widoku
interface DisplayInterface
{
	public function render();
	public function __toString();
	
	public function __set($name, $value);
    public function __get($name);
    public function __isset($name);
    public function __unset($name);
}

?>
