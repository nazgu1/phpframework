<?php
/* *
 * Cookie handler (to be replaced and merged with session)
 * * class/cookie.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */

class Cookie
{
	/**
     * Pobiera ciasteczko
     * 
     * @param String $name nazwa ciasteczka
     * 
     * @return mixed
     * @access public
     * @static
     */
	public static function get($name) {
		return (isset($_COOKIE[$name])) ? $_COOKIE[$name] : NULL;
	}
	
	/**
     * Ustawia ciasteczko
     * 
     * @param String $name nazwa ciasteczka
     * @param mixed $value dane ciasteczka
     * @param bool $http czy ciasteczko może być pobierane przez JS
     * 
     * @access public
     * @static
     */
	public static function set($name, $value, $http=true, $path=NULL, $force=false) {
		if($path==NULL)
			$path = c('framework_url');
			
		if($force || !isset($_COOKIE[$name]))
			setcookie($name, $value, time() + 36000000,
						$path, dc($_SERVER, 'HTTP_HOST', 'site_host'),
						isset($_SERVER['HTTPS']), $http);
	}
	
	/**
     * Usuwa ciasteczko
     *
     * @access public
     * @static
     */
	public static function destroy($name) {
		if(!isset($_COOKIE[$name]))
			setcookie($name, NULL, time() - 1);
	}
}

?>