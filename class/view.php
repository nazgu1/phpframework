<?php
/* *
 * Template (view) class
 * * class/view.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */

class View implements DisplayInterface
{
	/**
     * Nazwa szablonu
     *
     * @var String
     **/
	protected $_name;
	
	/**
     * Zmienne szablonu
     *
     * @var Array {name => value}
     **/
	protected $_vars = array();
	
	/**
     * Wyrenderowany szablon
     *
     * @var String
     **/
	protected $_output;

	/**
     * Konstruktor
     * 
     * @param String $name nazwa szablonu 
     * @access public
     */
	public function __construct($name)
	{
		$this->_name = $name;
		
		//set template basedir
		$this->_vars['base'] = '/';
		$this->_vars['user'] = NF\User::instance()->user();
	}
	
public function __set($name, $value)
{
    $this->_vars[$name] = $value;
}

public function __get($name) {
    if (array_key_exists($name, $this->_vars)) {
        return $this->_vars[$name];
    }
	return NULL;
}

public function __isset($name) {
    return isset($this->_vars[$name]);
}

public function __unset($name) {
    unset($this->_vars[$name]);
}

	/**
     * Przetwarza szablon
     * 
     * @access public
     */
	public function render($name=NULL)
	{
		//tworzymy ścieżkę do szablonu
		if(isset($name))
			$view_template = TEMPLATES_DIR.strtolower($name).TEMPLATES_EXT;
		else
			$view_template = TEMPLATES_DIR.strtolower($this->_name).TEMPLATES_EXT;
		
		//włączamy buforowanie wyjścia
		ob_start();
		//dołączamy plik szablonu
		if(file_exists($view_template))
			include $view_template;
		else
			throw new ViewException('Nie znaleziono strony', 404);
		//czyścimy bufor i zwracamy
		$ret  = ob_get_contents();
		ob_end_clean();
		$this->_output = $ret;
		return $ret;
	}
	
	/**
     * Zwraca ciąg znaków - wyrenderowany szablon
     * 
     * @return string
     * @access public
     */
	public function __toString()
	{
		try
		{
			return strval($this->render());
		}
		catch(Exception $e)
		{
			Error::Handle($e);
		}
	}
}

?>
