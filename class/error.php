<?php
/* *
 * Error handling
 * * inc/error.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	© 2011
 *
 * */ 
 
 //TODO: serve static files
 
class Error{
	static private $_error;

	private static function displayError()
	{
		if(Error::$_error->getCode()==404)
		{
			header('HTTP/1.1 404 Not Found');
			header('Status: 404 Not Found');
		}
		else if(Error::$_error->getCode()==500)
		{
			header('HTTP/1.1 500 Internal Error');
			header('Status: 500 Internal Error');
		}
	
		ob_start();
		$filename = TEMPLATES_DIR.'error'.TEMPLATES_EXT;

		$m = Error::$_error->getMessage();
		$title = l('Error');
		$status = Error::$_error->getCode();

		require_once($filename);
		$output = ob_get_contents();
		ob_end_clean();
		
		echo $output;
	}

	static function Handle($e)
	{
		//wyświetlamy użytkownikowi ekran z błędem
		Error::$_error = $e;
		Error::displayError();
		die(); //kończymy zabawę
	}

}


?>
