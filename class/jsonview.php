<?php
/* *
 * Template (view) class
 * * inc/jsonview.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */

class JSONView implements DisplayInterface
{
	/**
     * Nazwa szablonu
     *
     * @var String
     **/
	protected $_name;
	
	/**
     * Zmienne szablonu
     *
     * @var Array {name => value}
     **/
	protected $_vars = array();
	
	/**
     * Wyrenderowany szablon
     *
     * @var Array {name => value}
     **/
	protected $_output;

	/**
     * Konstruktor
     * 
     * @param String $name nazwa szablonu 
     * @access public
     */
	public function __construct($name)
	{
		$this->_name = $name;
	}
	
	public function __set($name, $value)
	{
        $this->_vars[$name] = $value;
    }
    
    public function __get($name) {
        if (array_key_exists($name, $this->_vars)) {
            return $this->_vars[$name];
        }
		//TODO: Throwing exception!
    }
    
    public function __isset($name) {
        return isset($this->_vars[$name]);
    }
    
    public function __unset($name) {
        unset($this->_vars[$name]);
    }

	/**
     * Przetwarza szablon
     * 
     * @access public
     */
	public function render($name=NULL)
	{
		return json_encode($this->_vars);
		return $ret;
	}
	
	/**
     * Zwraca ciąg znaków - wyrenderowany szablon
     * 
     * @return string
     * @access public
     */
	public function __toString()
	{
		try
		{
			return strval($this->render());
		}
		catch(Exception $e)
		{
			return json_encode($e);
		}
	}
}

?>
