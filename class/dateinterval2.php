<?php
/* *
 * Controller class
 * * class/dateinterval2.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */

/* *
 * based on http://www.php.net/manual/pl/dateinterval.format.php#102271 by kuzb 
 * */
class DateInterval2 extends DateInterval
{
	/**
	 * Zwraca sekundy intewału
	 * 
	 * @return int
	 * @access public
	 */
	public function to_seconds()
	{
		return ((((($this->y*365 + $this->m*30) + $this->d)*24 + $this->h)*60 + $this->i)*60) + $this->s;
	}
    
	/**
	* Przelicza interwał na odpowiednie wartosci lat, mcy itd
	* 
	* @return int
	* @access public
	*/
	public function recalculate()
	{
		$seconds  =  $this->to_seconds();
		$seconds -= ($this->y = floor($seconds/31536000)) * 31536000;
		$seconds -= ($this->m = floor($seconds/2592000)) * 2592000;
		$seconds -= ($this->d = floor($seconds/86400)) * 86400;
		$seconds -= ($this->h = floor($seconds/3600)) * 3600;
		$seconds -= ($this->i = floor($seconds/60))*60;
					 $this->s = 	  $seconds;
	}
}

?>