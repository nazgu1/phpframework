<?php
/* *
 * Route dispatcher
 * * inc/dispatcher.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */
 
class Dispatcher {
	protected $_pages;
	
	
	//konstruktor
	public function __construct() {
	}
	
	//pobieramy nazwe kontrolera/akcji dla podanego url i zwracamy parametry url
	private function getAction($route, &$params) {
		if(!isset($this->_pages))
			$this->_pages = require ROOT_DIR.'conf/routes.php';
		
		foreach($this->_pages as $pattern=>$path) { //dla wszystkich możiwych dróg
			if(preg_match($pattern, $route, $res)) { //sprawdzamy czy zapytanie pasuje do wzorca danej drogi
				$params = array_slice($res, 1);	//jesli tak to pobieramy wszystkie parametry
				return $path;	//i zwracamy wybrana droge
			}
		}
		
		//jesli nie ma dopasowania to rzucamy wyjątek
		throw new RouteException('There is no such controller', 404);;
	}
	
	//funkcja filtruje wejście (POST, GET, SERVER, REQUEST)
	private function filter() { 
		function f($item1) {
			if(is_array($item1))
				return array_map('f', $item1);

			$item1 = strip_tags($item1, '<p><a><br><br/><ul><li><hr><span><pre><code><strong><small><abbr>');
			$item1 = trim(str_replace(array('style=', '../', './', ''), '', $item1));
			$item1 = str_replace(array('"', '\''), array('&rdquo;', '&rsquo;'), $item1);
			
			if(is_numeric($item1)) //jesli liczba
			{
				if((int)$item1==(double)$item1) //jesli liczba nie jest typu double 
					return (int)$item1; //zwracamy int
				return (double)$item1; //zwracamy double
			}
			return $item1;
		}
	
		$_GET     = array_map('f', $_GET);
		$_POST    = array_map('f', $_POST);
		$_SERVER  = array_map('f', $_SERVER);
		$_REQUEST = array_map('f', $_REQUEST);
	}
	
	//obsluga pobrania adresu, parsowania go i dopasowania odpowiedniej akcji - dispatch
	public function route() {
		$route = da($_SERVER, 'REQUEST_URI', '/'); //pobieramy adres zapytania
		$route = explode('?', $route, 2);	//oddzielamy czesc zapytania get
		$route = $route[0];
		$route = str_ireplace(FRAMEWORK_URI, '', $route); //usuwamy fragment scieżki do aplikacji
		
		$this->filter(); //filtrujemy zmienne
		
		//dispatcher
		try {
			$params = array();	//tablica parametrów
			$page = $this->getAction($route, $params);	//pobieramy drogę, która ma pójsć wykonanie zapytania
			
			if($page==null) {	//jesli nie udało się znaleźć drogi
				throw new RouteException('No such URL', 404); //wyrzucamy wyjątek
				return;
			}
			
			$page['controller_path'] 	= CONTROLLERS_DIR.$page['controller'].'.php';	//ustawiamy ścieżkę do kontrolera
			$page['controller'] 		= ucfirst($page['controller']);	//i nazwe kontrolera
			if(!array_key_exists('strategy', $page))
				$page['strategy'] = 'html'; //jesli nie ma strategii to html jest domyslną
			
			if(!file_exists($page['controller_path'])) {	//jesli plik kontrolera nie istnieje
				throw new RouteException('There is no such controller', 404); //wyrzucamy wyjątek
				return;
			}
			
			if(!class_exists($page['controller'], false)) { //jesli odpowiednia klasa jeszcze nie została wczytana
				require($page['controller_path']);	//wczytujemy ją
			}
			
			$controller = new $page['controller']($page['strategy']); //tworzymy obiekt kontrolera
			
			//uruchamiamy akcję
			if(isset($controller)) //jesli udało sie stworzyć obiekt
			    $controller->callAction($page['action'], $params);
			else //jeśli nie
				throw new RouteException('There is no such action', 404); //rzucamy wyjątkiem
	
			//render template & print to stdout
			$controller->render();
			
			unset($controller); //usuwamy obiekt kontrolera

			//zapisujemy historie stron jakie odwiedził użytkownik i zapisujemy w jego sesji
			$backs = NF\Session::get('back');
			
			if(!is_array($backs))
				$backs = array('', '', '', '', '');
			$backs[] = substr($route, 1,strlen($route)-1);	
			array_shift($backs);
			NF\Session::set('back', $backs);
		}
		catch(Exception $e) {
			//jeśli błąd to obsługujemy go - wyswietlamy strone o błędzie w aplikacji	
			Error::Handle($e);
		} //try
	}
}


//obsluga pobrania adresu, parsowania go i dopasowania odpowiedniej akcji - dispatch
public function route() {
	$route = da($_SERVER, 'REQUEST_URI', '/'); //pobieramy adres zapytania
	$route = explode('?', $route, 2);	//oddzielamy czesc zapytania get
	$route = $route[0];
	$route = str_ireplace(FRAMEWORK_URI, '', $route); //usuwamy fragment scieżki do aplikacji
	
	$this->filter(); //filtrujemy zmienne
	
	//dispatcher
	try {
		$params = array();	//tablica parametrów
		$page = $this->getAction($route, $params);	//pobieramy drogę, która ma pójsć wykonanie zapytania
		
		if($page==null) {	//jesli nie udało się znaleźć drogi
			throw new RouteException('No such URL', 404); //wyrzucamy wyjątek
			return;
		}
		
		$page['controller_path'] 	= CONTROLLERS_DIR.$page['controller'].'.php';	//ustawiamy ścieżkę do kontrolera
		$page['controller'] 		= ucfirst($page['controller']);	//i nazwe kontrolera
		if(!array_key_exists('strategy', $page))
			$page['strategy'] = 'html'; //jesli nie ma strategii to html jest domyslną
		
		if(!file_exists($page['controller_path'])) {	//jesli plik kontrolera nie istnieje
			throw new RouteException('There is no such controller', 404); //wyrzucamy wyjątek
			return;
		}
		
		if(!class_exists($page['controller'], false)) { //jesli odpowiednia klasa jeszcze nie została wczytana
			require($page['controller_path']);	//wczytujemy ją
		}
		
		$controller = new $page['controller']($page['strategy']); //tworzymy obiekt kontrolera
		
		//uruchamiamy akcję
		if(isset($controller)) //jesli udało sie stworzyć obiekt
			$controller->callAction($page['action'], $params);
		else //jeśli nie
			throw new RouteException('There is no such action', 404); //rzucamy wyjątkiem
	
		//render template & print to stdout
		$controller->render();
		
		unset($controller);

?>
