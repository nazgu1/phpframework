<?php
/* *
 * Controller class
 * * class/controller.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */

class Controller {
	private   $name;		// nazwa kontrolera
	protected $display;		// zmienna z obiektem widoku
	private   $strategy;	// strategia wyswietlania (HTML, JSON, etc)

	/**
     * Konstruktor
     * 
     * @param String $strategy strategiua wyswoetlania (html, json, …)
     * @access public
     **/
	public function __construct($strategy = 'html')
	{
		if(!isset($strategy))
			$this->strategy = 'html'; //domyslnie html
		else
			$this->strategy = $strategy;
			
		//nazwa kontrolera to nazwa klasy która dziedziczy po tej
		$this->name = get_called_class();
	}
	
	/**
     * Wywołuje akcje kontrolera
     * 
     * @param String $action nazwa akcji
     * 
     * @access public
     */
	public function callAction($action, $params)
	{
		//sprawdzamy czy metoda istnieje
		//tworzynymy widok i wywołujemy akcje
		if(method_exists($this, $action)) {
			$this->display = new DisplayContext($this->name.'_'.$action, $this->strategy);
			$this->$action($params);
		}
		else {
			//nie ma takiej akcji, zgłaszamy wyjątek i kod błędu HTTP 404
			throw new ControllerException('There is no such action', 404);
		}
	}
	
	/**
     * Wysyła wyrenderowany szablon do użytkownika
     * 
     * @access public
     */
	public function render()
	{
		//wyrzucamy na wyjście (wywołuje metode widoku __toString())
		try{
			echo $this->display;
		}
		catch(Exception $e){
			//jesli błąd (błąd widoku) to go łapiemy i
			//wyswietlamy odpowiednią strone uzytkownikowi
			Error::Handle($e);
		}
	}

}

?>