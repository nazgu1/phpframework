<?php
/* *
 * Template helpers
 * * inc/helpers.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */
 
class Helpers {
	
	//przekierowanie bez sprawdzania czy jes URI powrotu
	function redirect_no($url='/', $response=302, $framework=true)
	{
		if($framework)
			$url = FRAMEWORK_URI.'/'.$url;
			
		header('Location: '.$url, TRUE, $response);
		exit;
	}

	//przekierowanie do strony z parametru, lub jesli istnieje to do URI powrotu
	function redirect($url='/', $response=302, $framework=true)
	{
		if(array_key_exists('redir', $_SESSION))
		{
			header('Location: '.$_SESSION['redir'], TRUE, $response);
			unset($_SESSION['redir']);
			exit;
		}
		
		Helpers::redirect_no($url, $response, $framework);
	}
}

?>
