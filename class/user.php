<?php
/* *
 * Site user class
 * * class/user.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 * @see UserSettings
 *
 * */
 
namespace NF;

class User
{
	protected $_user;
	private   $_loggedin;
	
	/**
     * Konstruktor klasy
     *
     * @access private
     */
	private function __construct()
	{
		
	}
	
	/**
     * Zwraca informacje czy użytkownik jest zalogowany
     *
     * @return bool
     * @access public
     */
	public function isLoggedIn() {
		if(!isset($this->_loggedin))
			$this->_loggedin = Session::get('loggedin');
		return $this->_loggedin;
	}
	
	/**
	 * This function generates a password salt as a string of x (default = 15) characters
	 * ranging from a-zA-Z0-9 and some others.
	 * @param $max integer The number of characters in the string
	 * @author AfroSoft <info@afrosoft.tk>
	 */
	private function generateSalt($max = 15) {
		$characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*(){}[]";
		$i = 0;
		$salt = "";
		do {
			$salt .= $characterList{mt_rand(0,strlen($characterList)-1)};
			$i++;
		} while ($i < $max);
		return $salt;
	}
	
	/**
     * Funkcja dodawana na początku akcji, sprawdza, czy uzytkownik jest zalogowany,
     * a jesli nie prziekierowuje do logowania
     *
     * @param $level integer Poziom uprawnien
     * @param $redir integer Czy przekierować do strony logowania
     * @return int ID użytkownika
     * @access public
     */
	public function requireLoggedin($level = 4, $redir=true) {
		$redir = $redir && (strpos(da($_SERVER, 'REQUEST_URI', '/'),'json')==FALSE) ;
		if(!$this->isLoggedIn()){
			if($redir){
				$route = da($_SERVER, 'REQUEST_URI', '/');
				Session::redir($route);
				\Helpers::redirect_no('user/login');
			}
		}
		else if($level < $this->user()->getIdusertype()){
			if($redir){
				Session::flash('Nie masz wystarczających uprawnień!', 'error');
				\Helpers::redirect_no('');
			}
		}
		
		return $this->user()->getIdusertype();
	}
	
	public function requireNotLoggedin() {
		if($this->isLoggedIn()) {
			Session::flash('Już jesteś zalogowany!', 'error');
			\Helpers::redirect('user');
			exit;
		}
	}
	
	public function user() {
		if($this->_user===NULL)
		{
			$uid = Session::get('userid');
			
			if($uid===FALSE)
				return NULL;
			
			$_usr = \DbuserQuery::create()
  			->findOneByIduser($uid);
  			
  			if($_usr==NULL)
  			{
  				$this->logout();
  				return;
  			}
  			
  			$this->_user = $_usr;
		}
		return $this->_user;
	}
	
	private function generatePassword($password, $salt)
	{
		return hash_hmac('sha512', str_rot13($salt.$password.$salt), PASSWORD_SECRET);
	}
	
	public function generateActivationId($mail, $salt)
	{
		return hash_hmac('sha512', base64_encode($mail.$salt), ID_SECRET);
	}
	
	/**
     * Zmienia hasło użytkownika
     *
     * @param $password	string nowe hasło użytkownika
     * @param $oldpassword	string stare hasło użytkownika
     * @return bool
     * @access public
     */
	public function changePassword($password, $oldpassword)
	{
		if(!$this->_user)
  			return false;
  		
  		if($this->_user->getPassword() == $this->generatePassword($oldpassword, $this->_user->getSalt()))
  		{
  			if($this->_user->getActivated())
  			{
	  			$this->_user->setPassword(
	  					$this->generatePassword($password, $this->_user->getSalt())
	  					);
	  			$this->_user->save();
	  			return true;
  			}
  			else
  				return false;
  		}
	
		return false;
	}
	
	public static function createFirstUser() {
		$_usr = new \Dbuser();
		$user['Email'] = 'admin@cycloo.pl';
		$user['Idusertype'] = 1; //set user type to normal user
		$salt = User::generateSalt();
		$user['Password'] = User::generatePassword('admin', $salt); //generate password
		$_usr->fromArray($user);
		$_usr->setDate('NOW'); //set (creation) register date
		$_usr->setSalt($salt); 
		$_usr->setActivated(1);
		$_usr->setIdcity(1);
		$_usr->setName(1);
		$_usr->setSurname('Admin');
		$_usr->save();
		$_priv = new \Dbprivacy();
		$_priv->setIduser($_usr->getPrimaryKey());
		$_priv->setIduser(0);
		$_priv->setRoutes(0);
		$_priv->setStatsprivate(0);
		$_priv->setSurname(0);
		$_priv->setEmail(0);
		$_priv->setCity(0);
		$_priv->setAge(0);
		$_priv->setNewsletter(0);
		$_priv->setStatsperiod(0);
		$_priv->save();
	}
	
	/**
     * Tworzy konto użytkownika
     *
     * @param $user	array	login,name,surname,city,mail
     * @return bool
     * @access public
     */
	public function register($user)
	{
		//dostajemy zwalidowane dane jedyne co to musimy wyszukac odpowiednie elementy slownikow i dodac usera
		//oraz sprawdzic wczesniej czy taki juz nie istnieje
		$_usr = new \Dbuser();
		
		//search for name
		$name = \DbfirstnameQuery::create()
  			->findOneByName($user['Name']);
  		if($name!==NULL)
 			$user['Name'] = $name->getId();
 		//else dodawanie nowego (choc nie wiem czy powinno to miec miejsce)
 			
 		//search for city
 		$city = \DbcityQuery::create()
  			->findOneByName($user['City']);
  		if($city!==NULL)
 			$user['Idcity'] = $city->getIdcity();
 		//else dodawanie nowego miasta i województwa (?)
		
		$user['Idusertype'] = 4; //set user type to normal user
		$salt = User::generateSalt();
		$user['Password'] = $this->generatePassword($user['Password'], $salt); //generate password
		$user['facebook'] = false;  //not registered by facebook
		$user['activated'] = false;

		$_usr->fromArray($user);
		$_usr->setDate('NOW'); //set (creation) register date
		$_usr->setSalt($salt); 
		$_usr->save();
		
		$_priv = new \Dbprivacy();
		$_priv->setIduser($_usr->getPrimaryKey());
		$_priv->setCity(1);
		$_priv->setSurname(1);
		$_priv->setRoutes(1);
		$_priv->setStatsprivate(1);
		$_priv->setEmail(1);
		$_priv->setAge(1);
		$_priv->setNewsletter(1);
		$_priv->setStatsperiod(1);
		$_priv->save();
		return $_usr;
	}
	
	/**
     * Loguje użytkownika, oraz zwraca informacje czy użytkownik jest zalogowany
     *
     * @return bool
     * @access public
     */
	public function login($user, $password)
	{
		Session::destroy();
		Session::start();

		$_usr = \DbuserQuery::create()
  			->findOneByEmail($user);

  		if(!$_usr)
  			return ($this->_loggedin = false);
  		
  		if($_usr->getPassword() == $this->generatePassword($password, $_usr->getSalt()))
  		{
  			if($_usr->getActivated())
  			{
	  			$this->_user = $_usr;
	  			Session::set('loggedin', true);
	  			Session::set('userid', $_usr->getIduser());
	  			return ($this->_loggedin = true);
  			}
  			else {
  				return ($this->_loggedin = false);
  			}
  		}
	
		return ($this->_loggedin = false);
	}
	
	/**
     * Wylogowuje użytkownika
     *
     */
	public function logOut()
	{
		Session::set('loggedin', false);
  		Session::set('userid', false);
		$this->_loggedin = false;
		$this->_user = false;
		unset($_SESSION);
		Session::destroy();
	}
	
	/**
     * Zwraca instancję obiektu User
     *
     * @return User
     * @access public
     * @static
     */
	static function &instance()
    {
        static $oInstance = false;
        if( $oInstance == false )
        {
            $oInstance = new User();
        }
        return $oInstance;
    }
}

?>