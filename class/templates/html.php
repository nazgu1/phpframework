<?php
/* *
 * Template HTML,JS,CSS helpers
 * * inc/helpers.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */

class HTML {
	public static function css($name) {
		if(!file_exists(STATIC_DIR.'stylesheets/'.$name))
			return;
			
		echo '<link rel="stylesheet" type="text/css" href="'.FRAMEWORK_URI.'/stylesheets/'.$name.'" />';
	}
	
	public static function js($name) {
		if(!file_exists(STATIC_DIR.'javascripts/'.$name))
			return;
			
		echo '<script type="text/javascript" src="'.FRAMEWORK_URI.'/javascripts/'.$name.'"></script>';
	}
	
	public static function image($name) {
		if(!file_exists(STATIC_DIR.'images/'.$name))
			return;
			
		echo FRAMEWORK_URI.'/images/'.$name;
	}
	
	public static function layout($name) {
		if(!file_exists(TEMPLATES_DIR.'layout/'.$name.TEMPLATES_EXT))
			return;
		require TEMPLATES_DIR.'layout/'.$name.TEMPLATES_EXT;
	}
	
	public static function header() {
		layout('header');
	}
	
	public static function footer() {
		layout('footer');
	}
	
	public static function mailto($mail) {
		$string = eregi_replace('([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})','<a href="mailto:\\1">\\1</a>', $mail);
		echo $string;
	}
}
?>