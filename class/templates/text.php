<?php
/* *
 * Template text helpers
 * * inc/texthelpers.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */

class TEXT {
	public static function shorten($subject, $charNumber, $useDots=false)
	{
		if(strlen($subject)<=$charNumber)
			return $subject;
		
		$subject = substr($subject.' ', 0, $charNumber);
		$subject = substr($subject, 0, strrpos($subject, ' '));
		
		if($useDots)
			$subject = $subject.'...';
		else
			$subject = $subject.'&#x2026;';
	
		return $subject;
	}
}
?>
