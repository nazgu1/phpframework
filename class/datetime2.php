
<?php
/* *
 * DateTime addon class
 * * class/datetime2.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */

class DateTime2 extends DateTime
{	
	/**
	 * Return a date string compatible with SQL queries
	 *
	 * @return string
	 */
	public function SQL()
	{
		return $this->format('%Y-%m-%d %H:%M:%S');
	}
	
	/**
	 * Overrides DateTime class format finction, and use strftime (it's localizable) so 
	 * it's incompatible with DateTime's format.
	 *
	 * @param string $format strftime() format string
	 * @return string formatted date
	 */
	public function format($format) {
		return strftime($format, $this->getTimestamp());
	}
}

?>