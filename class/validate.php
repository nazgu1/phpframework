<?php
/* *
 * Validation helper
 * * class/validator.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */

//TODO: More validation schemas
//TODO: File validation (size, extension, file type)

class Validate
{
	static public function is_same($one, $two)
	{
		return (strcmp($one,$two)===0);
	}

	static public function url($url)
	{
		return preg_match("#^http(s)?://[a-z0-9-_.]+\.[a-z]{2,4}#i",$url);
	}

	static public function email($email)
	{
		$email_pattern = "/^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,6})+$/";
	
		if(preg_match($email_pattern, $email))
			return true;
			
		return false;
	}
	
	static public function min_length($subject, $count)
	{
		if(strlen($subject)>=$count)
			return true;
			
		return false;
	}
	
	static public function max_length($subject, $count)
	{
		if(strlen($subject)<=$count)
			return true;
			
		return false;
	}
	
	static public function length($subject, $min, $max)
	{
		return Validate::max_length($subject, $max) && Validate::min_length($subject, $min);
	}
	
	static public function file_max_size($file_array, $size)
	{
		return ($file_array['size']<=$size);
	}
	
	//TODO: array of mimetypes
	static public function file_type($file_array, $mime)
	{
		if(is_array($mime))
		{
			foreach($mime as $m)
				if(mime_content_type($file_array['tmp_name'])==$m)
					return true;
			return false;
		}
		
		return (mime_content_type($file_array['tmp_name'])==$mime);
	}
}

?>