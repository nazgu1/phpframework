<?php
/* *
 * Mailer class 
 * * inc/mailer.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */
 
 //TODO: change encoding + iconv

class Mail{

	static public function Send($to, $subject, $type, $data)
	{
		$keys = array_keys($data);
		for ($i=0; $i<count($data); $i++) {
			$key = $keys[$i];
			global ${$key};
			${$key} = $data[$key];
		}
		
		ob_start();
		include(ROOT_DIR.'mailer/'.$type.'.php');
		$body = ob_get_contents();
		ob_end_clean();
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$headers .= 'From: Cycloo.pl <uzytkownicy@cycloo.pl>' . "\r\n";
		$headers .= 'Content-Transfer-Encoding: 8bit' . "\r\n";
				
		return mail($to, '=?UTF-8?B?'.base64_encode($subject).'?=', nl2br($body), $headers);
	}
}
?>