<?php
/* *
 * GPX files parsing class
 * * class/gpxfile.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia, Michał Stelmach
 * @copyright	(c) 2011
 *
 * */

class GPXPoint {
	private $lat;
	private $lon;
	private $elev;
	private $time;
	private $distance;
	private $speed;
	
	public function __construct($lat=0,$lon=0,$elev=0,$time=''){
		$this->lat  = $lat;
		$this->lon  = $lon;
		$this->elev = $elev;
		$this->time = $time;
	}
	
	public function setLat($lat) {
		$this->lat = (double)$lat;
	}
	public function setLon($lon) {
		$this->lon = (double)$lon;
	}
	public function setElevation($elev) {
		$this->elev = (double)$elev;
	}
	public function setTime($time) {
		$this->time = $time;
	}
	public function setDistance($dist) {
		$this->distance = $dist;
	}
	public function setSpeed($speed) {
	
		$this->speed = $speed;
	}
	
	public function getLon() {
		return $this->lon;
	}
	public function getLat() {
		return $this->lat;
	}
	public function getElevation() {
		return $this->elev;
	}
	public function getTime($seconds=false) {
		if($seconds)
			return NULL;
		return $this->time;
	}
	public function getDistance() {
		return $this->distance;
	}
	public function getSpeed() {
		return $this->speed;
	}
}
 
class GPXFile {
	private $path;
	private $name;
	private $points;
	private $creator;
	private $version;
	private $date;
	
	private $length;
	private $elevation;
	private $min_elev;
	private $max_elev;
	private $max_speed;
	private $time;
	
	private $parser;
	
	public function __construct($path)
	{
		if(!file_exists($path)) {
			throw new GPXException('Podany plik nie istnieje', 500);
			return null;
		}
		
		$this->path = $path;
		
		$this->parser = new GPXParser($this);
		$this->parser->process();
		unset($this->parser);
		
		if(count($this->points)==0) {
			throw new GPXException('Plik nie posiada ścieżki', 500);
			return null;
		}
		
		if ($this->points[0])
		{
			$this->max_speed = 0;
			$this->max_elev = 0;
	      	$this->min_elev = PHP_INT_MAX;
			
			$kmlength = 0.0;	//ile km przejechano do tej pory w odcinku km
			$km = 1;			//ktory km
			
			$totallength = 0;	//calkowita dlugosc drogi w metrach
   			$length = 0;		//dlugosc malego odcninka drogi
   			$R = 6371;			// promien równika km
   			$rad = 0.017453292519943; //stopnie razy to daja radiany
   			
   			$prevspeed = 0;		//predkosc punktu poprzedniego
			
			$mintime = $this->points[0]->getTime();
			$maxtime = $this->points[count($this->points)-1]->getTime();

			if($maxtime)
				$this->time = (int)abs($maxtime->getTimestamp() - $mintime->getTimestamp());
				
			$ile = count($this->points);
			define('GPX_MAX_POINTS', 250);
			if($ile>GPX_MAX_POINTS)
			{
				for($i=0;$i<$ile;$i+=($ile)/((float)$ile-GPX_MAX_POINTS))
				{
					unset($this->points[(int)$i]);
				}
				$this->points = array_values($this->points);
			}
			
			$pt = 0;
			$y = count($this->points);
			for ($x=0; $x<$y;$x++)
   			{	
   				$time = NULL;
   				//liczenie długości trasy
   				if($this->points[$x]->getTime())
   				{
   					if($this->points[$pt]->getTime())
						$time = abs((float)$this->points[$x]->getTime()->getTimestamp() - 
							$this->points[$pt]->getTime()->getTimestamp());
					else
						$time = 0;
						
					if($time<=10&&$time!=0){
						if($x!=$pt)
							unset($this->points[$x]);
						continue;
					}
				}
				
				//metoda liczenia odległosci haversine
				$dLat = ($this->points[$x]->getLat()-$this->points[$pt]->getLat())*$rad/2.0;
				$dLon = ($this->points[$x]->getLon()-$this->points[$pt]->getLon())*$rad/2.0;
				$lat1 = $this->points[$pt]->getLat()*$rad;
				$lat2 = $this->points[$x]->getLat()*$rad;
			
				$sin = sin($dLat);
				$sinp = $sin*$sin;
				$sin2 = sin($dLon);
				$sin2p = $sin2*$sin2;
				$a = $sinp+$sin2p*cos($lat1)*cos($lat2); 
				$c = 2.0*asin(min(1,sqrt($a))); 
	        	$length = $R*$c; //otrzymujemy odległośc w lini prostej między punktami GPS
	      
	        	$this->length += $length; //dodajemy ją do odległosci całkowitej
				$this->points[$x]->setDistance((float)$length); //i ustawiamy ją jako ogległość punktu
				
				if($time) { //jeśli czas jest zadeklarowany
					$speed = ($length*3600.0)/$time; //length (km); time (s); 
					$speed = ($prevspeed+$speed)/2.0;
					$this->points[$x]->setSpeed($speed);
					$prevspeed = $speed;
				}
				$pt = $x;
									
				//liczenie wysokości (min, max) na całej trasie
				if ($this->max_elev < $this->points[$x]->getElevation())
	          	{
	          		$this->max_elev = $this->points[$x]->getElevation();
				}
	         	if ($this->min_elev > $this->points[$x]->getElevation())
	          	{
	          		$this->min_elev = $this->points[$x]->getElevation();
				}
				
				if ($this->max_speed < $this->points[$x]->getSpeed())
				{
					$this->max_speed = $this->points[$x]->getSpeed();
				}
   			}
			//liczenie wysokości (roznicy) na całej trasie
			$this->elevation = abs($this->max_elev - $this->min_elev);
	   	}
	   	$this->points = array_values($this->points);
	}
	
	public function addPoint($point) {
		$this->points[] = $point;
	}
	
	// zwraca roznice wysokosci
	public function getElevation()
	{
	    return $this->elevation;
 	}
  
	public function getMaxElevation()
	{
		return $this->max_elev;
	}
  
	public function getMinElevation()
	{
		return $this->min_elev;
	}
	 
	public function getTime()
	{
		return $this->time;
	}

	public function getMaxSpeed()
	{
		return $this->max_speed;
	}
	
  //obliczanie dlugosci trasy
  public function getLength()
  { 
   return $this->length;
  }
		
	public function getPath()
	{
		return $this->path;
	}
	
	public function getPoints()
	{
  		return $this->points; 
  	}
  	
  	public function getDate()
  	{
  		if($this->date)
  			return $this->date;
  		return new DateTime2('NOW');
  	}
  	
  	public function setDate($date)
  	{
  		if($this->date==NULL)
  			$this->date = $date;
  	}
	
	public function setVersion($version)
	{
		$this->version = $version;
	}
	
	public function setCreator($creator)
	{
		$this->creator = $creator;
	}
	
	public function setName($name)
	{
		$this->name = $name;
	}
	
}
 
class GPXParser {
	private $gpxfile;
	private $current_tag;
	private $current_point;
	
	public function __construct($gpxfile)
	{
		$this->gpxfile = $gpxfile;
	}
	
	private function processtag_gpx($params)
	{
		if(array_key_exists('VERSION', $params))
			$this->gpxfile->setVersion($params['VERSION']);
		if(array_key_exists('CREATOR', $params))
			$this->gpxfile->setCreator($params['CREATOR']);
	}
	
	private function processtag_trkpt($params)
	{
		$this->current_point = new GPXPoint();
		if(array_key_exists('LAT', $params))
			$this->current_point->setLat((double)$params['LAT']);
		if(array_key_exists('LON', $params))
			$this->current_point->setLon((double)$params['LON']);
	}

	private function start_tag($parser, $attr, $params)
	{
		$this->current_tag = $attr;
		switch($attr) {
			case 'GPX':
				$this->processtag_gpx($params);
				break;
			case 'TRKPT':
				$this->processtag_trkpt($params);
				break;
		}
	}
     
	private function tag_text($parser, $text)
	{
		$text = str_replace(array("\r", "\n", '  '), '', $text);
		if(strlen($text) < 2)
			return;
				
		switch($this->current_tag)
		{
			case 'NAME':
				$this->gpxfile->setName($text);
				break;
			case 'ELE':
				if(isset($this->current_point))
					if (is_numeric($text))
						$this->current_point->setElevation((double)$text);
				break;
			case 'TIME':
				if(isset($this->current_point)){
					
					try {
						$date = new DateTime2($text);
					}
					catch (Exception $e) {
						$date = new DateTime2('0000-00-00 00:00:00');
					}
					$this->current_point->setTime($date);
					$this->gpxfile->setDate($date);
				}
				break;	
		}
	}
	
	private function end_tag($parser, $attr)
	{
		switch($attr)
		{
			case 'TRKPT':
				$this->gpxfile->addPoint($this->current_point);
				unset($this->current_point);
				break;
		}
   }
   
	public function process()
	{
		$parser = xml_parser_create();
	
		xml_set_element_handler($parser, array(&$this,'start_tag'), array(&$this,'end_tag'));
		xml_set_character_data_handler($parser, array(&$this,'tag_text'));
		
		//otwarcie pliku
		if(!($fp = fopen($this->gpxfile->getPath(), 'r')))
			throw new GPXException('Nie można otworzyć podanego pliku', 500);
		
		while($data = fread($fp, 4096))
		  if(!xml_parse($parser, $data, feof($fp)))
		  	throw new GPXException('Błąd xml: '.xml_get_error_code($parser).' (linia: '.xml_get_current_line_number($parser).')', 500);

		xml_parser_free($parser);
			
		
	}
}
	
?>