<?php
/* *
 * Polish language file
 * * lang/pl.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */
 
 //TODO: do it! ;)

$l = array();

//// Plural forms
//dla języka polskiego
//zwraca:
//[0] - drzewo
//[1] - drzewa
//[2] - drzew
$l['is_plural'] = 'if($c==1) return 0; elseif((($c%10>=2)&&($c%10<=4))&&(($c!=12)&&($c!=14))) return 1;else return 2;';

$l['year'][0] = 'rok';
$l['year'][1] = 'lata';
$l['year'][2] = 'lat';
$l['month'][0] = 'miesiąc';
$l['month'][1] = 'miesiące';
$l['month'][2] = 'miesięcy';
$l['day'][0] = 'dzień';
$l['day'][1] = 'dni';
$l['day'][2] = 'dni';
$l['hour'][0] = 'godzina';
$l['hour'][1] = 'godziny';
$l['hour'][2] = 'godzin';
$l['hour2'][0] = 'godzinę';
$l['hour2'][1] = 'godziny';
$l['hour2'][2] = 'godzin';
$l['minute'][0] = 'minuta';
$l['minute'][1] = 'minuty';
$l['minute'][2] = 'minut';
$l['second'][0] = 'sekunda';
$l['second'][1] = 'sekundy';
$l['second'][2] = 'sekund';

$l['ago'] = 'temu';
$l['now'] = 'teraz';
$l['in'] = 'za';

$l['months_short'] = array('','Sty','Lut','Mar','Kwi','Maj','Cze',
							'Lip','Sie','Wrz','Paź','Lis','Gru');
$l['months'] = array('','Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec',
						'Sierpień','Wrzesień','Październik','Listopad','Grudzień');
$l['date_format'] = '';

$l['Error'] = 'Błąd';
$l['Unknown controller'] = 'Nienznany kontroler';
$l['Controller file not exists'] = 'Plik kontrolera nie istnieje';
$l['Controller error'] = 'Błąd kontrolera';
$l['Controller has no such action'] = 'W kontrolerze nie istnieje podana akcja';

return $l;

?>