<?php
/* *
 * Engilsh language file
 * * lang/en.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */

// Names of PHP errors
$l = array();

//// Plural forms, e.g.
// [0] - tree
// [1] - trees
$l['is_plural'] = 'if($c<=1){ return 0;} else {return 1;}';

$l['year'][0] = 'year';
$l['year'][1] = 'years';
$l['month'][0] = 'month';
$l['month'][1] = 'months';
$l['day'][0] = 'day';
$l['day'][1] = 'days';
$l['hour'][0] = 'hour';
$l['hour'][1] = 'hours';
$l['minute'][0] = 'minute';
$l['minute'][1] = 'minutes';
$l['second'][0] = 'second';
$l['second'][1] = 'seconds';

$l['ago'] = 'ago';
$l['now'] = 'now';
$l['in'] = 'in';

$l['months_short'] = array('','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
$l['months'] = array('','January','February','March','April','May','June',
						'July','August','September','October','November','December');
$l['date_format'] = '';

$l['Unknown controller'] = 'Unknown controller';
$l['Controller file not exists'] = 'Controller file not exists';
$l['Controller error'] = 'Controller error';
$l['Controller has no such action'] = 'Controller has no such action';


return $l;

?>