<?php
/* *
 * Main system file
 * * index.php
 *
 * @package		NazgulFramework
 * @author		Dawid Dziurdzia
 * @copyright	(c) 2011
 *
 * */
 
//ROOT_DIR - scieżka do katalogu głównego strony
define('ROOT_DIR', realpath(dirname(__FILE__)). '/');

ob_start();

//ustawiamy strefę czasową na Europe/Warsaw
date_default_timezone_set('Europe/Warsaw');
setlocale(LC_ALL, 'pl_PL');

try {
	//Wczytujemy klase autoloadera
	require ROOT_DIR.'inc/exceptions.php';
	require ROOT_DIR.'inc/autoloader.php';
	//Wczytujemy podstawową konfiguracje frameworka
	require ROOT_DIR.'inc/functions.php';
	require ROOT_DIR.'inc/config.php';

	//report errors if Debug
	ini_set('error_log', __DEBUG_PATH);
	
	if(__DEBUG) {
		error_reporting(E_ALL);
		
		if(__DEBUG_DISPLAY)
			ini_set('display_errors', 1);
		else
			ini_set('display_errors', 0);	
	}
	else
		error_reporting(E_ERROR | E_WARNING);
	
	//rozpoczynamy nowa sesje
	NF\Session::start();
	
	// Include the main Propel script
	require_once ROOT_DIR.'libs/propel/runtime/lib/Propel.php';
	
	// Initialize Propel with the runtime configuration
	Propel::init(ROOT_DIR.'models/build/conf/ridestats-conf.php');
	
	// Add the generated 'classes' directory to the include path
	set_include_path(ROOT_DIR.'models/build/classes' .
						PATH_SEPARATOR . get_include_path());
				
	//propel debug		
	/*
	if(__DEBUG) {
		//$con = Propel::getConnection(DbuserPeer::DATABASE_NAME);
		//$con->useDebug(true);
	}
	*/
	
	//tworzymy obiekt dispatchera
	$dispatch = new Dispatcher();
	
	//kierujemy ruch
	$dispatch->route();
	
	//zwalniamy zasoby
	unset($dispatch);
	unset($autoloader);
	
	//zapisujemy sesję
	NF\Session::save();
	
	ob_end_flush();
}
catch (Exception $e) {
	Error::Handle($e);
}
?>